package ADTs.drawables;

import java.awt.*;
import java.util.HashMap;
import java.util.Map;

import static org.apache.log4j.Logger.getRootLogger;

/**
 * Created by Bassam on 12/7/2017.
 */
public abstract class DynamicDrawable extends Drawable {

    public static final int DIRECTION_UP = 0;
    public static final int DIRECTION_DOWN = 1;
    public static final int DIRECTION_RIGHT = 2;
    public static final int DIRECTION_LEFT = 3;
    private static final int DEFAULT_MOVING_DIRECTION = DIRECTION_RIGHT;
    private static final double DEFAULT_MOVING_SPEED_FACTOR = 0;

    private double movingSpeedFactor;
    private int movingDirection;
    private Map<Integer, String> imagesWithDirectionsMap;

    public DynamicDrawable() {
        this.movingSpeedFactor = DEFAULT_MOVING_SPEED_FACTOR;
        this.movingDirection = DEFAULT_MOVING_DIRECTION;
        this.imagesWithDirectionsMap = new HashMap();
    }

    public boolean moveTo(Point destinationPoint) {

        if (!isPointHorizontal(getCenterPoint(), destinationPoint) && !isPointVertical(getCenterPoint(), destinationPoint)) {
            getRootLogger().error("The point cannot be moved to - not horizontal nor vertical.");
            return false;
        }

        int movingDirection = findMovingDirection(getCenterPoint(), destinationPoint);
        setMovingDirection(movingDirection);
        setImageOfCurrentDirection(movingDirection);

        this.setCenterPoint(destinationPoint);

        return true;
    }

    private void setImageOfCurrentDirection(int movingDirection) {
        String imageNameOfThisDirection = imagesWithDirectionsMap.get(movingDirection);
        if (imageNameOfThisDirection != null) {
            setImageName(imageNameOfThisDirection);
        }
    }

    private int findMovingDirection(Point centerPoint, Point destinationPoint) {

        if (isPointHorizontal(centerPoint, destinationPoint)) {
            if (centerPoint.getX() - destinationPoint.getX() <= 0) {
                return DIRECTION_RIGHT;
            }
            return DIRECTION_LEFT;
        } else {
            if (centerPoint.getY() - destinationPoint.getY() <= 0) {
                return DIRECTION_UP;
            }
            return DIRECTION_DOWN;
        }
    }


    private boolean isPointVertical(Point centerPoint, Point destinationPoint) {
        return centerPoint.getX() == destinationPoint.getX();
    }

    private boolean isPointHorizontal(Point centerPoint, Point destinationPoint) {
        return centerPoint.getY() == destinationPoint.getY();
    }


    public static int getDefaultMovingDirection() {
        return DEFAULT_MOVING_DIRECTION;
    }

    public static double getDefaultMovingSpeedFactor() {
        return DEFAULT_MOVING_SPEED_FACTOR;
    }

    public double getMovingSpeedFactor() {
        return movingSpeedFactor;
    }

    public void setMovingSpeedFactor(double movingSpeedFactor) {
        this.movingSpeedFactor = movingSpeedFactor;
    }

    public int getMovingDirection() {
        return movingDirection;
    }

    public void setMovingDirection(int movingDirection) {
        this.movingDirection = movingDirection;
    }

    public void addImageWithDirection(int direction, String imageName) {
        imagesWithDirectionsMap.put(direction, imageName);
    }

    public String getImageOfDirection(int direction) {
        return imagesWithDirectionsMap.get(direction);
    }
}

